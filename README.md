# The NA64 Experiment Geometry Library

This repository keeps track the descriptions of NA64 experiment. Included
assets are:
* Hierarchical geometry of detectors, vessels, magnets, etc.
* Medium parameters of their volumes
* Visual attributes of described volumes
* Sensitive detector association mechanics
* Event display receptive volumes and digits association

The detector mapping files establishing correspondence between DATE/DDD digit
and particular detector entity are located in dedicated repository.

## Structure

The `tgdml/` directory hosts templated GDML descriptions. Templated GDML have
to be rendered using `render.py` script of
[ext.GDML package](https://bitbucket.org/CrankOne/ext.gdml) prior to being
used by Geant4 as a pure GDML. However, for convinience, master branch could
contain a `gdml.out/` directory with rendered geometrical presets. Also,
the `tgeo.out/` dir in master branch contains `.root` files where geometry is
rendered into ROOT::TGeo format for convinience.
