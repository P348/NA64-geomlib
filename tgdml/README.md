# Geometry Library

This library contains geometrical description of detectors taking part in
NA64 experiment. The organization of this directory is claimed by
[extGDML package](https://bitbucket.org/CrankOne/ext.gdml) performing all the
pre-processing operations on the GDML templates.

To contribute detector description, please take note the recommendations given
within this README file.

## Motivation and Library Structure

### Primary Template Files

At the root directory of this distro the following files are located:

* The `00_root.tgdml` file represents a generic template that has to be
used for most of the dynamically-composed GDML documents.
* The `01_defines.tgdml` file represents a part of GDML document that contains
common definitions for the entire project. Common constants, sizes and measures
have to be put here. This template will be rendered into `01_defines.igdml`
location (file or URL) reflecting the fact that is not a standalone GDML
document.
* The `02_materials.tgdml` file represents a part of GDML document that contains
common material definitions for the entire project. This template will be
rendered into `02_materials.igdml` location.
* The `03_solids.tgdml` file represents a part of GDML document that contains
common solids definitions for the entire project (actually, it does not imply
many use cases except for world volume). This template will be rendered into
`03_solids.igdml` location.
* The `04_structures/` directory contains a somewhat generic variants of GDML
common structure definitions for the entire project. This template will be
rendered into `04_structure/<whatever>.igdml` location by demand.

The contributors would probably change (or just refer to) the numerical
and material definitions (`01_defines.tgdml`, `02_materials.tgdml`). Using of
other parts is not expected to be frequent neccessary. See "Consuming Software"
below for reference of how this library is supposed to be used.

### The `.igdml` Files

Each time one desires to gain the particular setup in GDML format, the same
data has to be present in different places many times. E.g., it is convinient
to split full setup description into few smaller files representing particular
detectors. However each detector will involve the same materials and
numerical definitions. To avoid duplication of such sections we introduce the
template substitution based on Jinja2 template rendering engine. We put the
common definitions and materials into dedicated files and include them into the
detector files by demand. These blocks are not the standalone GDML documents
and marked with the `.igdml` extension. One may find them in the root
directory here.

### Detector Descriptions

Depending on particular simulation task, some geometrical traits of the
particular detector geometry may or may not be important, so it is
crucial to provide all the available variants within the geometry lib.
To avoid mess, we make a dedicated subdirectories for them, thus the detectors
have their own subdirectories right at the root folder. Many of them are
presented in few variants. The following naming is accepted by consuming
software:

    <detector-name>/<detector_name>[_<detector_variant>].[i]gdml

Examples:

    ECAL/ECAL_nosegm.tgdml
    ECAL/ECAL.gdml
    BGO/BGO_one.gdml

The 't' in extension (`.tgdml`) is recommended, however the `.gdml` will also
be understood since some detector description came as software-generated
output and has to be compatible.

However, the various geometry description files that do not present the
specific detector may recede this rule. For example, the `vessels/` and `misc/`
directory may contain arbitrary files and subdirectories.

## Consuming Software

This library may be utilized in two major ways:

1. As an input to ext.GDML python package that produces file structure of pure
`.gdml` files. Just use the rendering script from ext.GDML distribution to
generate this local GDML library.
2. As a set of templates that will be dynamically consumed by
[ext.GDML](https://bitbucket.org/tpu_elementary_particles_lab/ext.gdml)
blueprint of [StromaV](https://github.com/CrankOne/StromaV)'s
[resources server](https://bitbucket.org/CrankOne/sv-resources) to perform highly-parameterized
generation of GDML geometry description distributed over HTTP protocol.

The first way is good for unobtrusive local development, but the second way
(once resources server being running) provides much more flexibility.

### Note About Parsers

Some XML parsers (mainly amataur projects like ROOT's XML parser) does not
support XML `DOCTYPE/!ENTITY` definition. We cover this use case by
introducing conditional includes that has to be performed using the template
engine, but utilization `DOCTYPE/!ENTITY` is more natural way to perform
literal include. The presense of is controlled by `restrainedXML` rendering
context flag.

## Recommendations for Contributors

### Sandwiches

Note: some of us were unable to obtain this formula by themselves, so I'm
now having to write this note. Suppose, one has a sandwich of
layers. Each layer $i$ has own width $w\_i$. The longitudal distance of
sandwich from its center to center of layer $n$, thus, can be obtained:

    d_n = ( \sum_{i < n} w_i - \sum_{j > n} w_j )/2

Sandwich-like geometry needs that to define where the centers of layers will
be placed.

